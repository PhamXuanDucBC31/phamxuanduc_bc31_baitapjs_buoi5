function tinh_tong(a, b, c, d, e) {
  return a + b + c + d + e;
}
function check_student() {
  let std = document.getElementById("standard").value * 1;
  let a = document.getElementById("object").value * 1;
  let b = document.getElementById("area").value * 1;
  let c = document.getElementById("diem1").value * 1;
  let d = document.getElementById("diem2").value * 1;
  let e = document.getElementById("diem3").value * 1;
  if (tinh_tong(a, b, c, d, e) < std) {
    document.getElementById(
      "result_check_student"
    ).innerHTML = `<div> Bạn đã rớt, điểm của bạn là ${tinh_tong(
      a,
      b,
      c,
      d,
      e
    )} </div>`;
  } else {
    document.getElementById(
      "result_check_student"
    ).innerHTML = `<div> Bạn đã đậu, điểm của bạn là ${tinh_tong(
      a,
      b,
      c,
      d,
      e
    )} </div>`;
  }
  if (c == 0 || d == 0 || e == 0) {
    document.getElementById(
      "result_check_student"
    ).innerHTML = `<div> Bạn đã rớt, do có 1 hoặc nhiều môn 0 điểm </div>`;
  }
}
function electric(x) {
  if (x <= 50) {
    return x * 500;
  }
  if (50 < x && x <= 100) {
    return 50 * 500 + (x - 50) * 650;
  }
  if (100 < x && x <= 200) {
    return 50 * 500 + 50 * 650 + (x - 100) * 850;
  }
  if (200 < x && x <= 350) {
    return 50 * 500 + 50 * 650 + 100 * 850 + (x - 200) * 1100;
  }
  if (350 < x) {
    return 50 * 500 + 50 * 650 + 100 * 850 + 150 * 1100 + (x - 350) * 1300;
  }
}
function money_electric() {
  let e_name = document.getElementById("myname").value;
  let e_kw = document.getElementById("kw").value * 1;

  document.getElementById(
    "result_electric"
  ).innerHTML = `<div> Tiền điện tháng này của ${e_name} là ${new Intl.NumberFormat(
    "vie-VN",
    {
      style: "currency",
      currency: "VND",
    }
  ).format(electric(e_kw))} </div>`;
}
function income_tax(income, person) {
  var s = income - 4e6 - person * 1.6e6;
  if (s <= 60e6) {
    return s * 0.05;
  }
  if (60e6 < s && s <= 120e6) {
    return s * 0.1;
  }
  if (120e6 < s && s <= 210e6) {
    return s * 0.15;
  }
  if (210e6 < s && s <= 384e6) {
    return s * 0.2;
  }
  if (384e6 < s && s <= 624e6) {
    return s * 0.25;
  }
  if (624e6 < s && s <= 960e6) {
    return s * 0.3;
  }
  if (960e6 < s) {
    return s * 0.35;
  }
}
function tax() {
  let t_name = document.getElementById("tax_name").value;
  let t_income = document.getElementById("tax_income").value * 1;
  let t_pers = document.getElementById("tax_pers").value * 1;

  document.getElementById(
    "result_tax"
  ).innerHTML = `<div> Tiền thuế của ${t_name} là ${new Intl.NumberFormat(
    "vie-VN",
    {
      style: "currency",
      currency: "VND",
    }
  ).format(income_tax(t_income, t_pers))} </div>`;
}

